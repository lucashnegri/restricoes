Leia-me
=======

Exemplos introdutórios utilizando a biblioteca python-constraint.
A biblioteca é simples, e provavelmente teremos que utilizar uma mais
completa (como o gecode). Mesmo assim, é interessante aprender o tópico com
ela, pois é mais fácil de instalar e utilizar.

Exercícios
----------

Implementem os seguintes problemas:

1) Liste as soluções para o quadrado mágico de lado 3 (https://pt.wikipedia.org/wiki/Quadrado_m%C3%A1gico)
2) Problema das 8 rainhas (https://en.wikipedia.org/wiki/Eight_queens_puzzle)
3) SEND + MORE = MONEY (http://gecoder.org/examples/send-more-money.html)