"""
Problema do quadrado mágico 3x3. Consiste em um quadro NxN que contém os números 
inteiros 1, 2, ..., N², dispostos (sem repetição) de forma com que os somatórios
de cada coluna / linha / diagonal sejam iguais.
"""

import constraint as ct

problema = ct.Problem()

# variáveis e seus domínios
faixa = range(1, 10)     # Células contém números de 1 até 9
variaveis = range(1, 10) # Variáveis identificadas pelos números de 1 até 9
problema.addVariables(variaveis, faixa)

# restrições
problema.addConstraint(ct.AllDifferentConstraint(), variaveis) # os valores não se repetem
soma_magica = 15

# linhas
problema.addConstraint(ct.ExactSumConstraint(soma_magica), [1, 2, 3])
problema.addConstraint(ct.ExactSumConstraint(soma_magica), [4, 5, 6])
problema.addConstraint(ct.ExactSumConstraint(soma_magica), [7, 8, 9])

# colunas
problema.addConstraint(ct.ExactSumConstraint(soma_magica), [1, 4, 7])
problema.addConstraint(ct.ExactSumConstraint(soma_magica), [2, 5, 8])
problema.addConstraint(ct.ExactSumConstraint(soma_magica), [3, 6, 9])

# diagonais
problema.addConstraint(ct.ExactSumConstraint(soma_magica), [1, 5, 9])
problema.addConstraint(ct.ExactSumConstraint(soma_magica), [3, 5, 7])

# mostrar as soluções
for i, sol in enumerate(problema.getSolutions()):
    print(f'\n** Solução {i+1} **')
    
    for l in range(3):
        for c in range(3):
            print(sol[l * 3 + c + 1], end=' ')
            
        print() 
