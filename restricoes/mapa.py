"""
Problema da coloração de mapas com 3 cores.

Mapa (territórios possuem conexão com os vizinhos)

a b   
c d f h
  e g

Colorir o mapa utilizando 4 cores, sendo que a cor do território "a"
e do território "h" deve ser 1.

"""

import constraint as ct

problema = ct.Problem()

# variáveis e seus domínios
cores = [1, 2, 3, 4]
borda = [1]

grafo = {
    "a": ["b", "c", "d"],
    "b": ["c", "d"],
    "c": ["d"],
    "d": ["e", "f", "g"],
    "e": ["c"],
    "f": ["e", "g", "h"],
    "g": ["e"],
    "h": ["g"],
}

for territorio in grafo:
    if territorio in ("a", "h"):
        problema.addVariable(territorio, borda)
    else:
        problema.addVariable(territorio, cores)

# restrições
for territorio, adjacencias in grafo.items():
    for adj in adjacencias:
        problema.addConstraint(lambda t, a: t != a, (territorio, adj))

# calcular as soluções
sols = problema.getSolutions()

# saída
print("# de soluções: ", len(sols))
if len(sols) > 0:
    print("Primeira solução:")
    print(sols[0])
