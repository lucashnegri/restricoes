"""
Eu sou quatro vezes mais velho que a minha filha.
Em vinte anos, eu terei duas vezes a idade dela.
Responda: Quantos anos nós temos agora?
"""

import constraint as ct

problema = ct.Problem()

# variáveis e seus domínios
faixa = range(1, 101)  # de 1 até 100 anos
problema.addVariable("pai", faixa)
problema.addVariable("filha", faixa)

# restrições
problema.addConstraint(lambda p, f: p == 4 * f, ("pai", "filha"))
problema.addConstraint(lambda p, f: p + 20 == 2 * (f + 20), ("pai", "filha"))

# mostrar as soluções
print(problema.getSolutions())
