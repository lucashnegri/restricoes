"""
    SEND
+   MORE
   -----
   MONEY

- Os valores devem ser distintos
- O primeiro dígito de cada número não pode ser 0
"""

import constraint as ct

problema = ct.Problem()

# variáveis e seus domínios
faixa = range(0, 10)     # Dígitos vão de 0 até 9
variaveis = ['S', 'E', 'N', 'D', 'M', 'O', 'R', 'Y']
problema.addVariables(variaveis, faixa)

# restrições
def equacao(s, e, n, d, m, o, r, y):
    a = s*1000 + e*100 + n*10 + d
    b = m*1000 + o*100 + r*10 + e
    c = m*10000 + o*1000 + n*100 + e*10 + y
    return a + b == c

problema.addConstraint(ct.AllDifferentConstraint())
problema.addConstraint(lambda d: d != 0, ('S'))
problema.addConstraint(lambda d: d != 0, ('M'))
problema.addConstraint(equacao, variaveis)

# mostrar as soluções
for i, sol in enumerate(problema.getSolutions()):
    print(f'\n** Solução {i+1} **')
    
    print('  {}{}{}{}'.format(sol['S'], sol['E'], sol['N'], sol['D'])) 
    print('+ {}{}{}{}'.format(sol['M'], sol['O'], sol['R'], sol['E']))
    print(' -----')
    print(' {}{}{}{}{}'.format(sol['M'], sol['O'], sol['N'], sol['E'], sol['Y']))