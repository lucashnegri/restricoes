import constraint as ct

problema = ct.Problem()

# variáveis e seus domínios
problema.addVariable("a", [1, 2, 3])
problema.addVariable("b", [4, 5, 6])

# restrições
problema.addConstraint(lambda a, b: a * 2 == b, ("a", "b"))
problema.addConstraint(lambda a: a > 2, "a")

# mostrar as soluções
print(problema.getSolutions())
