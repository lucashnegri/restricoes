"""
Problema das 8 rainhas.
"""

import constraint as ct

problema = ct.Problem()

# variáveis e seus domínios
faixa = range(1, 9)      # Coluna que a rainha está (1 até 8)
variaveis = range(1, 9)  # Um total de 8 rainhas
problema.addVariables(variaveis, faixa)

# restrições

# -> devido à modelagem, naturalmente só temos uma rainha por linha

# -> não podem se atacar nas colunas (devem estar em colunas distintas)
problema.addConstraint(ct.AllDifferentConstraint())

# -> não podem se atacar nas diagonais
def diagonais(*rainhas):
    for l, c in enumerate(rainhas, 1):
        for l2, c2 in enumerate(rainhas, 1):
            if l != l2 and abs(l2 - l) == abs(c2 - c):
                return False
    
    return True
problema.addConstraint(diagonais, variaveis)

# mostrar as soluções
for i, sol in enumerate(problema.getSolutions()):
    print(f'\n** Solução {i+1} **')
    
    for l in range(1, 9):
        for c in range(1, 9):
            print('|' + (sol[l] == c and '*' or ' '), end='')
            if c == 8:
                print('|')
